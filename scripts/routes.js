app.config(function ($stateProvider) {
    var main = {
        name: "main",
        url: "/",
        templateUrl: "/partials/main.html"
    };

    var architectureDesign = {
        name: "architecture-design",
        url: "/architecture/design",
        templateUrl: "/partials/architecture_design.html",
        controller: 'architectureDesignCtrl'
    };

    var architectureSupervision = {
        name: "architecture-supervision",
        url: "/architecture/supervision",
        templateUrl: "/partials/architecture_supervision.html",
        controller: "architectureSupervisionCtrl"
    };

    var about = {
        name: "about",
        url: "/about",
        templateUrl: "/partials/about_us.html",
        controller: "aboutCtrl"
    };

    var energyEfficiency = {
        name: "energy-efficiency",
        url: "/energy-efficiency",
        templateUrl: "/partials/energy_efficiency.html",
        controller: "energyEfficiencyCtrl"
    };

    var noiseBarriersInstallation = {
        name: "noise-barriers-installation",
        url: "/noise-barriers/installation",
        templateUrl: "/partials/noise_barriers_installation.html",
        controller: "noiseBarriersInstallationCtrl"
    };

    var noiseBarriersProduction = {
        name: "noise-barriers-production",
        url: "/noise-barriers/production",
        templateUrl: "/partials/noise_barriers_production.html",
        controller: "noiseBarriersProductionCtrl"
    };

    var noiseBarriersProductionProduct = {
        name: "noise-barriers-production-product",
        url: "/noise-barriers/production/{productId}",
        templateUrl: "/partials/noise_barriers_production_product.html",
        controller: "noiseBarriersProductionProductCtrl"
    };

    var contact = {
        name: "contact",
        url: "/contact",
        templateUrl: "/partials/contact.html",
        controller: "contactCtrl"
    };

    var newsAndMedia = {
        name: "news-and-media",
        url: "/news-and-media",
        templateUrl: "/partials/news_and_media.html",
        controller: "newsAndMediaCtrl"
    };

    $stateProvider.state(main);
    $stateProvider.state(architectureDesign);
    $stateProvider.state(architectureSupervision);
    $stateProvider.state(about);
    $stateProvider.state(energyEfficiency);
    $stateProvider.state(noiseBarriersInstallation);
    $stateProvider.state(noiseBarriersProduction);
    $stateProvider.state(noiseBarriersProductionProduct);
    $stateProvider.state(contact);
    $stateProvider.state(newsAndMedia);
});