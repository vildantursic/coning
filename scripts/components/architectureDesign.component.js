function architectureDesignCtrl ($scope) {

    var ctrl = this;

    console.log("OK");

    ctrl.projects = [
        {
            code: "private_house_sweden",
            name: "Private house, Sweden",
            client: "Private",
            project: "Private house",
            location: "Sweden",
            area: "156sqm",
            teamMembers: [
                "Mirjana Vukojevic",
                "Dalila Salkanovic",
                "Adna Kljajic"
            ],
            year: "2016",
            service: "Preliminary Design",
            imageLocation: "/images/architecture/design/private_house_sweden/",
            images: [
                "2.jpg",
                "3.jpg",
                "4.jpg"
            ]
        },
        {
            code: "reconstruction_of_cultural_center_trebinje",
            name: "Reconstruction of Cultural Center Trebinje",
            client: "TIKA",
            project: "Reconstruction of Cultural Center Trebinje",
            location: "Trebinje, Bosnia & Herzegovina",
            area: "156sqm",
            teamMembers: [
                "Mirsad Hodzic",
                "Mirjana Vukojevic",
                "Adem Hajrovic",
                "Sulejman Suljic"
            ],
            year: "2016",
            service: "Main Design",
            imageLocation: "/images/architecture/design/reconstruction_of_cultural_center_trebinje/",
            images: [
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg"
            ]
        },
        {
            code: "social_housing_sweden",
            name: "Social housing, Sweden",
            client: "Private",
            project: "Social housing",
            location: "Trosa, Sweden",
            area: "300sqm",
            teamMembers: [
                "Mirjana Vukojevic",
                "Dalila Salkanovic",
                "Adna Kljajic"
            ],
            year: "2016",
            service: "Preliminary Design",
            imageLocation: "/images/architecture/design/social_housing_sweden/",
            images: [
                "2.jpg",
                "3.jpg",
                "4.jpg"
            ]
        }
    ];
};

app.component('architectureDesignComponent', {
    templateUrl: '/partials/architecture_design.html',
    controller: architectureDesignCtrl
});