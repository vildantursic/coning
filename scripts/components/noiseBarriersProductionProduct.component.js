function noiseBarriersProductionProductCtrl($scope, $element, $attrs) {

    var ctrl = this;

    ctrl.projects = [
        {
            id: 0,
            name: "Noise Barrier Kohlhauer Alufera",
            image: "/images/noise_barriers/production/header_img.jpg",
            text: ""
        },
        {
            id: 1,
            name: "Noise Barrier Kohlhauer Alufera",
            image: "/images/noise_barriers/production/header_img.jpg",
            text: ""
        },
        {
            id: 2,
            name: "Noise Barrier Kohlhauer Alufera",
            image: "/images/noise_barriers/production/header_img.jpg",
            text: ""
        },
        {
            id: 3,
            name: "Noise Barrier Kohlhauer Alufera",
            image: "/images/noise_barriers/production/header_img.jpg",
            text: ""
        },
        {
            id: 4,
            name: "Noise Barrier Kohlhauer Alufera",
            image: "/images/noise_barriers/production/header_img.jpg",
            text: ""
        },
        {
            id: 5,
            name: "Noise Barrier Kohlhauer Alufera",
            image: "/images/noise_barriers/production/header_img.jpg",
            text: ""
        }
    ];
};

app.component('noiseBarriersProductionProductComponent', {
    templateUrl: '/partials/noise_barriers_production_product.html',
    controller: noiseBarriersProductionProductCtrl
});
