var app = angular.module("app", ["ui.router", "pascalprecht.translate"]);

app.config(function ($translateProvider) {
    $translateProvider.translations('en');
    $translateProvider.translations('bs');
    $translateProvider.useStaticFilesLoader({
        prefix: '/translations/',
        suffix: '.json'
    });
    $translateProvider.preferredLanguage('en');
});

app.controller("architectureDesignCtrl", function ($scope, $timeout) {
    $scope.itemInfo = {};

    $scope.projects = [
        {
            code: "private_house_sweden",
            name: "Private house, Sweden",
            client: "Private",
            project: "Private house",
            location: "Sweden",
            area: "156sqm",
            teamMembers: [
                "Mirjana Vukojevic",
                "Dalila Salkanovic",
                "Adna Kljajic"
            ],
            year: "2016",
            service: "Preliminary Design",
            imageLocation: "/images/architecture/design/private_house_sweden/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg"
            ]
        },
        {
            code: "reconstruction_of_cultural_center_trebinje",
            name: "Reconstruction of Cultural Center Trebinje",
            client: "TIKA",
            project: "Reconstruction of Cultural Center Trebinje",
            location: "Trebinje, Bosnia & Herzegovina",
            area: "156sqm",
            teamMembers: [
                "Mirsad Hodzic",
                "Mirjana Vukojevic",
                "Adem Hajrovic",
                "Sulejman Suljic"
            ],
            year: "2016",
            service: "Main Design",
            imageLocation: "/images/architecture/design/reconstruction_of_cultural_center_trebinje/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg"
            ]
        },
        {
            code: "social_housing_sweden",
            name: "Social housing, Sweden",
            client: "Private",
            project: "Social housing",
            location: "Trosa, Sweden",
            area: "300sqm",
            teamMembers: [
                "Mirjana Vukojevic",
                "Dalila Salkanovic",
                "Adna Kljajic"
            ],
            year: "2016",
            service: "Preliminary Design",
            imageLocation: "/images/architecture/design/social_housing_sweden/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg"
            ]
        },
        {
            code: "private_house_danderyd_sweden",
            name: "Private house Denderyd, Sweden",
            client: "Private",
            project: "Social housing",
            location: "Trosa, Sweden",
            area: "300sqm",
            teamMembers: [
                "Mirjana Vukojevic",
                "Dalila Salkanovic",
                "Adna Kljajic"
            ],
            year: "2016",
            service: "Preliminary Design",
            imageLocation: "/images/architecture/design/private_house_danderyd_sweden/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg"
            ]
        }
    ];

    // $timeout(function () {
    //     $scope.projects.forEach(function (project) {
    //         lightGallery(document.getElementById(project.code));
    //     })
    // }, 500);

    $timeout(function () {
        $(document).ready(function(){
            $(".first-project-slider").slick({
                dots: false,
                arrows: false,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 3000,
                speed: 1000,
                slidesToShow: 1,
                centerMode: true,
                variableWidth: true
            });
            $(".second-project-slider").slick({
                dots: false,
                arrows: false,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 3000,
                speed: 1000,
                slidesToShow: 1,
                centerMode: true,
                variableWidth: true,
                swipeToSlide: true
            });
        });
    }, 100);

    var show = 0;
    var el = document.getElementById("pdp");

    $scope.showProjectDescriptionPopup = function (item) {
        $scope.itemInfo = item;

        if (show === 0) {
            el.className += " pdp-show";
            show = 1;

            $timeout(function () {
                $(document).ready(function(){
                    $(".popup-slider").slick({
                        dots: false,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 1,
                        centerMode: true,
                        variableWidth: true,
                        swipeToSlide: true
                    });
                });
            }, 100);
        }
    };
    $scope.hideProjectDescriptionPopup = function () {
        $scope.itemInfo = {};

        if (show === 1) {
            el.className = "project-description-popup";
            show = 0;

            $('.popup-slider').slick("unslick");
        }
    }
});

app.controller("architectureSupervisionCtrl", function ($scope, $timeout) {
    $scope.itemInfo = {};

    $scope.projects = [
        {
            code: "reconstruction_of_bascarsija_square",
            name: "Reconstruction of Bascarsija square",
            client: "TIKA",
            project: "Reconstruction of Bascarsija square",
            location: "Old Town Sarajevo, Bosnia & Herzegovina",
            area: "156sqm",
            teamMembers: [
                "Mirsad Hodzic",
                "Salem Music"
            ],
            year: "2015",
            service: "Supervision",
            imageLocation: "/images/architecture/supervision/reconstruction_of_bascarsija_square/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg",
                "7.jpg",
                "8.jpg",
                "9.jpg",
                "10.jpg"
            ]
        },
        {
            code: "reconstruction_of_cultural_center_trebinje",
            name: "Reconstruction of Cultural Center Trebinje",
            client: "TIKA",
            project: "Trebinje, Bosnia & Herzegovina",
            location: "Old Town Sarajevo, Bosnia & Herzegovina",
            area: "156sqm",
            teamMembers: [
                "Mirsad Hodzic",
                "Salem Music"
            ],
            year: "2016",
            service: "Supervision",
            imageLocation: "/images/architecture/supervision/reconstruction_of_cultural_center_trebinje/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg"
            ]
        }
    ];

    var show = 0;
    var el = document.getElementById("pdp");

    $scope.showProjectDescriptionPopup = function (item) {
        $scope.itemInfo = item;

        if (show === 0) {
            el.className += " pdp-show";
            show = 1;

            $timeout(function () {
                $(document).ready(function(){
                    $(".popup-slider").slick({
                        dots: false,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 1,
                        centerMode: true,
                        variableWidth: true,
                        swipeToSlide: true
                    });
                });
            }, 100);
        }
    };
    $scope.hideProjectDescriptionPopup = function () {
        $scope.itemInfo = {};

        if (show === 1) {
            el.className = "project-description-popup";
            show = 0;

            $('.popup-slider').slick("unslick");
        }
    }
});

app.controller("energyEfficiencyCtrl", function ($scope, $timeout) {
    $scope.itemInfo = {};

    $scope.projects = [
        {
            code: "elementary_school_hspahic_ilijas",
            name: "Elementary School Hasim Spahic Ilijas",
            status: "Completed in October 2016",
            area: "2,539 m2",
            estimatedInvestment: "268,158.16 BAM",
            estimatedSavings: "51,427.00 BAM",
            energyEfficiencyMeasures: [
                "Windows and doors replacement",
                "External wall insulation",
                "Roof insulation",
                "Rehabilitation of heat substations / installation of heating performance measures",
                "Supply and installation of thermostatic valves",
                "Hydraulic balancing of the heating system",
                "Supply and installation of three frequency controlled pumps for the heating system",
                "EE lightening system improvements"
            ],
            imageLocation: "/images/energy_efficiency/elementary_school_hspahic_ilijas/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg"
            ]
        },
        {
            code: "general_hospital_tesanj",
            name: "General Hospital Tešanj",
            status: "Completed in December 2016",
            area: "5,844 m2",
            estimatedInvestment: "731,565.63 BAM",
            estimatedSavings: "168,646.00 BAM",
            energyEfficiencyMeasures: [
                "Windows and doors replacement",
                "External wall insulation",
                "Ceiling rehabilitation toward unheated attic",
                "Heating sources and energy products replacement",
                "Thermostatic valves installation",
                "Heat energy meters installation",
                "Roof thermal insulation"
            ],
            imageLocation: "/images/energy_efficiency/general_hospital_tesanj/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg",
                "7.jpg",
                "8.jpg"
            ]
        },
        {
            code: "elementary_school_mccatic_zenica",
            name: "Elementary School Musa Ćazim Ćatić Zenica",
            status: "Completed in November 2016",
            area: "3,423 m2",
            estimatedInvestment: "Estimated investment for energy efficiency measures (BAM with VAT): 461,116.01 BAM",
            estimatedSavings: "38,656.00 BAM",
            energyEfficiencyMeasures: [
                "Windows and doors replacement",
                "Balancing the central heating system",
                "Facade thermal insulation"
            ],
            imageLocation: "/images/energy_efficiency/elementary_school_mccatic_zenica/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg",
                "7.jpg",
                "8.jpg",
                "9.jpg",
                "10.jpg"
            ]
        },
        {
            code: "elementary_school_hkresevljakovic_kakanj",
            name: "Elementary School Hamdija Kresevljakovic Kakanj",
            status: "Completed in November 2016",
            area: "3,120 m2",
            estimatedInvestment: "Estimated investment for energy efficiency measures (BAM with VAT): 327,346.51 BAM",
            estimatedSavings: "58,333.00 BAM",
            energyEfficiencyMeasures: [
                "Wall and slant roof ceiling insulation",
                "Windows and doors replacement",
                "Improving the performance of the central system"
            ],
            imageLocation: "/images/energy_efficiency/elementary_school_hkresevljakovic_kakanj/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg",
                "7.jpg",
                "8.jpg"
            ]
        },
        {
            code: "first_primary_school_zavidovici",
            name: "First Primary School Zavidovici",
            status: "Completed in November 2016",
            area: "2,620 m2",
            estimatedInvestment: "344,647.69 BAM",
            estimatedSavings: "11,360.00 BAM",
            energyEfficiencyMeasures: [
                "Facade and sloped roof ceiling thermal insulation",
                "Windows and doors replacement",
                "Improving the performance of the central system (installation of thermostatic valves, calorimeters, Boiler regulation according to the outside temperature, balancing the central heating system and compact substation installation",
                "Simple lighting upgrade from incandescent to fluorescent with no works on electrical network"
            ],
            imageLocation: "/images/energy_efficiency/first_primary_school_zavidovici/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg"
            ]
        },
        {
            code: "elementary_school_srednje_ilijas",
            name: "Elementary School Srednje Ilijas",
            status: "Completed in October 2016",
            area: "2,346 m2",
            estimatedInvestment: "Estimated investment for energy efficiency measures (BAM with VAT): 489,380.40 BAM",
            estimatedSavings: "66,044.00 BAM",
            energyEfficiencyMeasures: [
                "Windows and doors replacement",
                "Installation of thermostatic valves",
                "Additional heating equipment/ radiators installation",
                "Boiler and energy products replacement",
                "Lighting replacement"
            ],
            imageLocation: "/images/energy_efficiency/elementary_school_srednje_ilijas/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg"
            ]
        },
        {
            code: "iv_elementary_school_mostar",
            name: "IV Elementary School School",
            status: "Completed in December 2016",
            area: "4,230 m2",
            estimatedInvestment: "791,967.78 BAM",
            estimatedSavings: "103,353.00 BAM",
            energyEfficiencyMeasures: [
                "Thermal insulation of an exterior walls",
                "Insulation of flat roof",
                "Insulation of sloping roof",
                "Insulation of the ceiling above heated space",
                "Replacing openings",
                "Replacement of the boiler with new pellet boilers, replacement of energy"
            ],
            imageLocation: "/images/energy_efficiency/iv_elementary_school_mostar/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg",
                "7.jpg"
            ]
        },
        {
            code: "health_center_jablanica",
            name: "Health Center Jablanica",
            status: "Completed in December 2016",
            area: "1,800 m2",
            estimatedInvestment: "256,670.92 BAM",
            estimatedSavings: "91,331.00 BAM",
            energyEfficiencyMeasures: [
                "Improving the thermal properties of the outer shell facade",
                "Improving the thermal properties of external openings-windows and doors",
                "Improving the properties of the central heating system of space-replacement equipment",
                "Supply, delivery and installation of hot water boiler for firing wood pellets"
            ],
            imageLocation: "/images/energy_efficiency/health_center_jablanica/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg"
            ]
        },
        {
            code: "high_school_konjic",
            name: "High School Konjic",
            status: "Completed in December 2016",
            area: "5,115 m2",
            estimatedInvestment: "887,039.83 BAM",
            estimatedSavings: "186,530.00 BAM",
            energyEfficiencyMeasures: [
                "Improvement of the thermal properties of the outer wall",
                "Repair a flat impassable and passable roof",
                "Replacement of the existing windows, doors and windows",
                "Replacement of the boiler with new pellet boilers, replacement of energy"
            ],
            imageLocation: "/images/energy_efficiency/high_school_konjic/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg",
                "7.jpg",
                "8.jpg",
                "9.jpg",
                "10.jpg",
                "11.jpg"
            ]
        },
        {
            code: "elementary_school_zbarucija_vogosca",
            name: "Elementary School Zahid Baručija Vogosca",
            status: "Completed in October 2016",
            area: "1,782 m2",
            estimatedInvestment: "268,158.16 BAM",
            estimatedSavings: "51,427.00 BAM",
            energyEfficiencyMeasures: [
                "Facade thermal insulation (EPS, 10 cm)",
                "Flat roof reconstruction with thermal insulation (XPS, 10 cm)",
                "Windows and doors replacement",
                "Daylight sensor installation",
                "Cleaning of the heating equipment in the school gym"
            ],
            imageLocation: "/images/energy_efficiency/elementary_school_zbarucija_vogosca/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg",
                "7.jpg"
            ]
        }
    ];

    var show = 0;
    var el = document.getElementById("pdp");

    $scope.showProjectDescriptionPopup = function (item) {
        $scope.itemInfo = item;

        if (show === 0) {
            el.className += " pdp-show";
            show = 1;

            $timeout(function () {
                $(document).ready(function(){
                    $(".popup-slider").slick({
                        dots: false,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 1,
                        centerMode: true,
                        variableWidth: true,
                        swipeToSlide: true
                    });
                });
            }, 100);
        }
    };
    $scope.hideProjectDescriptionPopup = function () {
        $scope.itemInfo = {};

        if (show === 1) {
            el.className = "project-description-popup";
            show = 0;

            $('.popup-slider').slick("unslick");
        }
    }
});

app.controller("noiseBarriersInstallationCtrl", function ($scope, $timeout) {
    $scope.itemInfo = {};

    $scope.projects = [
        {
            code: "motorway_prnjavor_doboj",
            name: "Motorway Prnjavor-Doboj",
            quantity: "33.500 m²",
            status: "Completed in September 2016",
            typeOfPanel: [
                "Kohlhauer Gmbh Alufera",
                "Kohlhauer Gmbh Scorsa"
            ],
            imageLocation: "/images/noise_barriers/installation/motorway_prnjavor_doboj/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg",
                "5.jpg",
                "6.jpg",
                "7.jpg"
            ]
        },
        {
            code: "sarajevo_bypass_lot_2",
            name: "Sarajevo Bypass Lot 2",
            quantity: "2.700 m²",
            status: "Completed in February 2016",
            typeOfPanel: [
                "Plexiglas Soundstop XT",
                "Plexiglas Soundstop GSCC",
                "Kohlhauer Gmbh Alufera"
            ],
            imageLocation: "/images/noise_barriers/installation/sarajevo_bypass_lot_2/",
            images: [
                "1.jpg",
                "2.jpg",
                "3.jpg",
                "4.jpg"
            ]
        },
        {
            code: "motorway_vlakovo_tarcin",
            name: "Motorway Vlakovo-Tarcin",
            quantity: "11.600 m²",
            status: "Completed in September 2014",
            typeOfPanel: [
                "Aluminium"
            ],
            imageLocation: "/images/noise_barriers/installation/motorway_vlakovo_tarcin/",
            images: [
            ]
        },
        {
            code: "sarajevo_bypass_lot_1",
            name: "Sarajevo Bypass Lot 1",
            quantity: "6.300 m²",
            status: "Completed in September 2013",
            typeOfPanel: [
                "Kohlhauer Gmbh Alufera"
            ],
            imageLocation: "/images/noise_barriers/installation/motorway_vlakovo_tarcin/",
            images: [
            ]
        },
        {
            code: "coca_cola_hbc_sarajevo",
            name: "Coca Cola HBC Sarajevo",
            quantity: "50 m²",
            status: "Completed in December 2013",
            typeOfPanel: [
                "Kohlhauer Gmbh Alufera"
            ],
            imageLocation: "/images/noise_barriers/installation/coca_cola_hbc_sarajevo/",
            images: [
            ]
        },
        {
            code: "university_clinical_centre_sarajevo",
            name: "University Clinical Centre Sarajevo",
            quantity: "160 m²",
            status: "Completed in December 2014",
            typeOfPanel: [
                "Kohlhauer Gmbh Alufera"
            ],
            imageLocation: "/images/noise_barriers/installation/university_clinical_centre_sarajevo/",
            images: [
            ]
        }
    ];

    var show = 0;
    var el = document.getElementById("pdp");

    $scope.showProjectDescriptionPopup = function (item) {
        $scope.itemInfo = item;

        if (show === 0) {
            el.className += " pdp-show";
            show = 1;

            $timeout(function () {
                $(document).ready(function(){
                    $(".popup-slider").slick({
                        dots: false,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 1,
                        centerMode: true,
                        variableWidth: true,
                        swipeToSlide: true
                    });
                });
            }, 100);
        }
    };
    $scope.hideProjectDescriptionPopup = function () {
        $scope.itemInfo = {};

        if (show === 1) {
            el.className = "project-description-popup";
            show = 0;

            $('.popup-slider').slick("unslick");
        }
    }
});

app.controller("noiseBarriersProductionCtrl", function ($scope) {
    $scope.products = [
        {
            id: "kohlhauer_alufera",
            name: "Kohlhauer Alufera",
            image: "/images/noise_barriers/production/kohlhauer_alufera/1.jpg",
            text: ""
        },
        {
            id: "kohlhauer_alumna",
            name: "Kohlhauer Alumna",
            image: "/images/noise_barriers/production/kohlhauer_alumna/1.jpg",
            text: ""
        },
        {
            id: "kohlhauer_ligna",
            name: "Kohlhauer Aigna",
            image: "/images/noise_barriers/production/kohlhauer_ligna/1.jpg",
            text: ""
        },
        {
            id: "kohlhauer_planta",
            name: "Kohlhauer Planta",
            image: "/images/noise_barriers/production/kohlhauer_planta/1.jpg",
            text: ""
        },
        {
            id: "kohlhauer_scorsa",
            name: "Kohlhauer Scorsa",
            image: "/images/noise_barriers/production/kohlhauer_scorsa/1.jpg",
            text: ""
        },
        {
            id: "kohlhauer_volta",
            name: "Kohlhauer Volta",
            image: "/images/noise_barriers/production/kohlhauer_volta/1.jpg",
            text: ""
        }
    ];
});

app.controller("noiseBarriersProductionProductCtrl", function ($scope, $stateParams) {

    $scope.products = [
        {
            id: "kohlhauer_alufera",
            name: "Kohlhauer Alufera",
            image: "/images/noise_barriers/production/kohlhauer_alufera/1.jpg",
            fileLocation: "/images/noise_barriers/production/kohlhauer_alufera/files/",
            files: [
                "ce-sign_alufera_en.pdf",
                "kohlhauer_alufera_brochure_en.pdf"
            ],
            info: [
                {
                    title: "",
                    content: "After the successful introduction of the patented plantable system KOHLHAUER PLANTA®, KOHLHAUER® is presenting the new all-round Grid-Insulation-System (GIS) KOHLHAUER ALUFERA®. Based on the proven design of our PLANTA system, this element had been specially developed for the needs of the new trans-european future markets, without having to renounce the proven construction principle. The result is a new and attractive large-sized noise barrier element, with the well-known and highly efficient acoustical properties and the easy to assemble, patented technology."
                },
                {
                    title: "Application",
                    content: "KOHLHAUER ALUFERA® noise barrier systems perfectly combine an appealing finish with sustainable construction and high technical requirements. All KOHLHAUER® Grid-Insulation-Systems (GIS) have been tested by renowned institutes for noise insulation, sound absorptivity, wind load, frost and road salt resistance, durability etc. and so KOHLHAUER ALUFERA® as well."
                },
                {
                    title: "Benefits",
                    list: [
                        "Innovative aluminium sectional frame for increased stability",
                        "Solid tongue and groove interlock for easy stacking of multiple elements",
                        "More solid vertical bars for even higher noise barriers",
                        "Ventilation and drain ducts for constant noise barrier performance and water drainage",
                        "Innovative click-fastening system for various post profiles reduces the assembly time effort at the construction site",
                        "Modular standard components for a cost efficient realisation of large-scale projects",
                        "Various height compensation modules (stackable) for easy adaptation to different landscapes",
                        "Extensive colour range for all system components allows an individual design",
                        "CE-certified quality"
                    ]
                },
                {
                    title: "Specifications",
                    list: [
                        "Excellent sound-insulating and highly absorptive performance on both sides",
                        "Solid element structure for a consistent natural appearance",
                        "All used materials are completely recyclable",
                        "Extremely high aging and corrosion resistance",
                        "High UV and colour resistance for all used materials",
                        "Combinable with other KOHLHAUER® noise barrier elements"
                    ]
                },
                {
                    title: "Assembly",
                    content: "The system's patent-pending fastening technology makes it possible to assemble the wall's elements without the need for screws, drilling or other processing of the wall's posts. This not only significantly reduces the time needed for assembly, it also protects the galvanized or otherwise coated wall elements from scratching and subsequent corrosion."
                }
            ]
        },
        {
            id: "kohlhauer_alumna",
            name: "Kohlhauer Alumna",
            image: "/images/noise_barriers/production/kohlhauer_alumna/1.jpg"
        },
        {
            id: "kohlhauer_ligna",
            name: "Kohlhauer Aigna",
            image: "/images/noise_barriers/production/kohlhauer_ligna/1.jpg"
        },
        {
            id: "kohlhauer_planta",
            name: "Kohlhauer Planta",
            image: "/images/noise_barriers/production/kohlhauer_planta/1.jpg"
        },
        {
            id: "kohlhauer_scorsa",
            name: "Kohlhauer Scorsa",
            image: "/images/noise_barriers/production/kohlhauer_scorsa/1.jpg"
        },
        {
            id: "kohlhauer_volta",
            name: "Kohlhauer Volta",
            image: "/images/noise_barriers/production/kohlhauer_volta/1.jpg"
        }
    ];
    $scope.product = {};

    $scope.products.forEach(function (product) {
        if (product.id === $stateParams.productId) {
            $scope.product = product;
        }
    })

});

app.controller("aboutCtrl", function ($scope) {
    $scope.about = {
        intro: {
            lines: [
                "CONING SARAJEVO was founded in 2009 as limited liability Company for design, construction, supervision, engineering and consulting.   The Company has earned superb references in the wide spectrum of various use buildings. Our clients are private persons, legal entities, State institutions and local municipalities.",
                "Our main objective is to invest our maximum commitment and professional knowledge to ensure that our clients’ interests are at the forefront of everything we do. ",
                "Our Company is active in a number of areas.    We started as a young and prosperous design bureau, employing architects with vast experience in design and management. It was not long before we expanded our operations to supervision, engineering and consulting, which expanded our field of work to construction engineering design and Environmental protection.  The Company currently operates in two significant departments – Architecture and Environmental Protection."
            ]
        },
        architecture: {
            title: "Architecture",
            lines: [
                "Architecture department focuses on Design, Supervision and Consultancy related to all construction works. This department employs engineers with extensive experience at all design levels and construction management of numerous facilities in Bosnia and Herzegovina, as well as internationally. "
            ]
        },
        environmentalProduction: {
            title: "Environmental Production",
            lines: [
                "Environmental protection department includes Energy Efficiency and Noise protection solutions. All EnC member states, including B&H, are obliged to make reduction in final energy consumption by 9% compared to base state in 2010, in period 2011-2018. Also, the energy sector in Bosnia and Herzegovina was recognized as one of the most important driving forces of B&H economy. Our services include preliminary and detailed energy audit, design, supervision and consultancy for improvement of Energy Efficiency in buildings.",
                "When it comes to Noise protection CONING is the leader in the field of noise barrier systems in Bosnia and Herzegovina. As being partner of R.Kohlhauer GmbH, with CONING you are assured a team of competent design, engineering and production experts in the field of sound protection and environment management. In close cooperation with leading manufacturers we can offer a wide product range to meet all requirements."
            ]
        },
        qualityPolicy: {
            title: "Quality Policy",
            text: "Our primary postulates in terms of quality assurance are:",
            lines: [
                "Quality knows no alternative",
                "Quality must be assured by each employee and the Company",
                "Invest all efforts to meet principal objectives – complete work in time, within budget plan with quality assurance provided every step of the way",
                "Compete tasks and meet all client deadlines",
                "Make progress in the field on a daily basis through specific experience, trainings, programs and seminars"
            ]
        },
        experience: {
            title: "Experience",
            lines: [
                "Our employees have important both local and international experience while working on projects in different areas and cooperating with companies from Germany, Slovenia, Sweden, Turkey, Pakistan, Slovenia,  Croatia, Serbia and others. We are proud to say that all of our current clients are extremely satisfied with our services, even more they were glad to provide us with letters of recommendation for future clients."
            ]
        }

    }
});

app.controller("contactCtrl", function ($scope) {

    var uluru = {lat: 43.856706, lng: 18.382338};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });

    $scope.company = {
        name: "CONING doo SARAJEVO",
        address: "Radnicka 70A, 71000 Sarajevo",
        country: "Bosnia & Herzegovina",
        phone: "+387 (0)33 711 300",
        fax: "+387 (0)33 711 301",
        email: "info@coning.ba"
    };

    $scope.contacts = [
        {
            name: "KENAN BUTUROVIC",
            title: "Founder & Director",
            phone: "+387 (0)33 711 300",
            mobile: "+387 (0)61 216 431",
            email: "kenan@coning.ba"
        },
        {
            name: "MIRJANA VUKOJEVIC",
            title: "Architecture",
            phone: "+387 (0)33 645 045",
            mobile: "+387 (0)62 272 921",
            email: "mirjana@coning.ba"
        },
        {
            name: "RAMIZ ARNAUT",
            title: "Noise barriers",
            phone: "+387 (0)33 711 300",
            mobile: "+387 (0)61 235 039",
            email: "info@coning.ba"
        },
        {
            name: "NAIDA BUTUROVIC",
            title: "Finance",
            phone: "+387 (0)33 711 300",
            mobile: "+387 (0)62 604 989",
            email: "finansije@coning.ba"
        }
    ]
});

app.controller("newsAndMediaCtrl", function ($scope) {
    $scope.news = [
        {
            id: "m_kvadrat",
            name: "TITLE",
            imageLocation: "/images/news_and_media/m_kvadrat/"
        },
        {
            id: "put_plus",
            name: "TITLE",
            imageLocation: "/images/news_and_media/put_plus/"
        }
    ];
});

app.controller("headerCtrl", function ($scope, $translate) {

    var flag = 0;
    var menu = document.getElementById("mobile-menu");
    $scope.showMenu = function () {
        if (flag === 0) {
            menu.className = "mobile-navigation mobile-navigation-show";
            flag = 1;
        } else {
            menu.className = "mobile-navigation";
            flag = 0;
        }
    };

    $scope.changeLanguage = function (key) {
        $translate.use(key);
    };
});

// var audio = new Audio('/images/audio.mp3');
// audio.play();